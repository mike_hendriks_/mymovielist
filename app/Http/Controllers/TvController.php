<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tmdb\Laravel\Facades\Tmdb;
use Illuminate\Support\Facades\Auth;


class TvController extends Controller
{
    public function detail($tv_id)
    {
        $tv = Tmdb::getTvApi()->getTvShow($tv_id);




        $tv['watchlist'] = $this->check_in_watchlist($tv , 'tv');
        $trailers = Tmdb::getTvApi()->getVideos($tv_id)['results'];
        $cast = Tmdb::getTvApi()->getCredits($tv_id);
        $similar = Tmdb::getTvApi()->getSimilar($tv_id)['results'];
        $similar_filtered = array();

        // print("<pre>".print_r($trailers ,true)."</pre>");exit;

        if (Auth::check()) {
            foreach ($similar as $similar_tv) {
                if (isset($similar_tv['backdrop_path']) && isset($similar_tv['poster_path'])) {
                    $watchlist = $this->check_in_watchlist($similar_tv, 'tv');
                    $similar_tv['watchlist'] = $watchlist;
                    $similar_filtered[] = $similar_tv;
                }
            }
        } else {
            $similar_filtered = $similar;
        }

        return view('tv_detail', ['tv' => $tv, 'trailers' => $trailers, 'cast' => $cast, 'similar' => $similar_filtered]);
    }
}
