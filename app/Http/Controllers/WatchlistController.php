<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tmdb\Laravel\Facades\Tmdb;
use App\Watchlist;

class WatchlistController extends Controller
{
    public function index()
    {
        $user_id = Auth::id();
        $watchlist = Watchlist::where('user_id', $user_id)->get();
        $discover = Tmdb::getDiscoverApi()->discoverMovies()['results'];
        $header_img = $discover[array_rand($discover, 1)]['backdrop_path'];

        return view('watchlist', ['header' => $header_img, 'watchlist' => $watchlist]);
    }

    public function add(Request $request)
    {
        $watchlist_item = new Watchlist;

        $watchlist_item->user_id = Auth::id();
        $watchlist_item->type = $request->input('type');
        $watchlist_item->item_id = $request->input('item_id');
        $watchlist_item->item_title = $request->input('item_title');
        $watchlist_item->item_img = $request->input('item_img');
        $watchlist_item->item_url = $request->input('item_url');

        $watchlist_item->save();
    }

    public function remove(Request $request)
    {
        Watchlist::where([
            'user_id' => Auth::id(),
            'type' => $request->input('type'),
            'item_id' => $request->input('item_id'),
            ])->delete();

    }
}
