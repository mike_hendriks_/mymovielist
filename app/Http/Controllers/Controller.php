<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Tmdb\Laravel\Facades\Tmdb;
use Illuminate\Support\Facades\Auth;
use Socialite;
use App\Watchlist;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $now_playing = Tmdb::getMoviesApi()->getNowPlaying()['results'];
        $upcoming_movies = Tmdb::getMoviesApi()->getUpcoming()['results'];
        $airing_today_tv = Tmdb::getTvApi()->getAiringToday()['results'];
        $now_playing_filtered = array();
        $upcoming_movies_filtered = array();
        $airing_today_tv_filtered = array();

        if (Auth::check()) {
            foreach ($now_playing as $movie) {
                if (isset($movie['backdrop_path']) && isset($movie['poster_path'])) {
                    $watchlist = $this->check_in_watchlist($movie, 'movie');
                    $movie['watchlist'] = $watchlist;
                    $now_playing_filtered[] = $movie;
                }
            }
            foreach ($upcoming_movies as $movie) {
                if (isset($movie['backdrop_path']) && isset($movie['poster_path'])) {
                    $watchlist = $this->check_in_watchlist($movie, 'movie');
                    $movie['watchlist'] = $watchlist;
                    $upcoming_movies_filtered[] = $movie;
                }
            }
            foreach ($airing_today_tv as $tv) {

                if (isset($tv['backdrop_path']) && isset($tv['poster_path'])) {
                    $watchlist = $this->check_in_watchlist($tv, 'tv');
                    $tv['watchlist'] = $watchlist;
                    $airing_today_tv_filtered[] = $tv;
                }
            }
        } else {
            $now_playing_filtered = $now_playing;
            $upcoming_movies_filtered = $upcoming_movies;
            $airing_today_tv_filtered = $airing_today_tv;
        }

        $content = array(
            'now_playing_movies' => $now_playing_filtered,
            'upcoming_movies' => $upcoming_movies_filtered,
            'airing_today_tv' => $airing_today_tv_filtered,
        );
        return view('home', $content);
    }

    public function check_in_watchlist($item, $type)
    {
        $watchlist = 0;
        if( Watchlist::where(['user_id' => Auth::id(), 'type' => $type, 'item_id' => $item['id']])->first() ){
            $watchlist = 1;
        } else {
            $watchlist = 0;
        }
        return $watchlist;
    }
}
