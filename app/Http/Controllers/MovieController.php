<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tmdb\Laravel\Facades\Tmdb;
use Illuminate\Support\Facades\Auth;


class MovieController extends Controller
{
    public function detail($movie_id)
    {
        $movie = Tmdb::getMoviesApi()->getMovie($movie_id);
        $movie['watchlist'] = $this->check_in_watchlist($movie , 'movie');
        $trailers = Tmdb::getMoviesApi()->getTrailers($movie_id)['youtube'];
        $cast = Tmdb::getMoviesApi()->getCredits($movie_id);
        $similar = Tmdb::getMoviesApi()->getSimilar($movie_id)['results'];
        $similar_filtered = array();

        if (Auth::check()) {
            foreach ($similar as $similar_movie) {
                if (isset($similar_movie['backdrop_path']) && isset($similar_movie['poster_path'])) {
                    $watchlist = $this->check_in_watchlist($similar_movie, 'movie');
                    $similar_movie['watchlist'] = $watchlist;
                    $similar_filtered[] = $similar_movie;
                }
            }
        } else {
            $similar_filtered = $similar;
        }

        return view('movie_detail', ['movie' => $movie, 'trailers' => $trailers, 'cast' => $cast, 'similar' => $similar_filtered]);
    }
}
