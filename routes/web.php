<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index');

// Pages
Route::get('/movie/{id}', 'MovieController@detail');
Route::get('/tv/{id}', 'TvController@detail');


// OAuth Routes
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

// Route::group(['middleware' => 'auth' ], function () {
// });
Auth::routes();
Route::get('watchlist', 'WatchlistController@index')->middleware('auth');
Route::post('watchlist/add_to_watchlist', 'WatchlistController@add');
Route::post('watchlist/remove_from_watchlist', 'WatchlistController@remove');

// Route::get('/home', 'HomeController@index')->name('home');
