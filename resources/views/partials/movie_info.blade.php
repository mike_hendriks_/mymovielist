<!-- INFO CONTAINER -->
<div class="row">
    <div class="col s12">
        <h3>Facts</h3>
    </div>
    <div class="col s12 m6">
        <h5 class="no_margin_top">Release date</h5>
        <?php
		if ($movie['release_date']){
            echo '<span>'.$movie['release_date'].'</span>';
        } else{
            echo '<span>-</span>';
        }
		?>

        <h5>Status</h5>
        <?php if ($movie['status']){
            echo '<span>'. $movie['status'] .'</span>';
		} else{
            echo '<span>-</span>';
		} ?>

        <h5>Runtime</h5>
        <?php if ($movie['runtime']){
            echo '<span>'. sprintf("%d Hours %02d Minutes",   floor($movie['runtime']/60), $movie['runtime']%60) .'</span>';
		} else{
            echo '<span>-</span>';
        } ?>

        <h5>Budget</h5>
        <?php if ($movie['budget']){
            echo '<span>$'. number_format($movie['budget'], 0, ',', '.') .'</span>';
        } else{
            echo '<span>-</span>';
        } ?>

        <h5>Revenue</h5>
        <?php if ($movie['revenue']){
            echo '<span>$'. number_format($movie['revenue'], 0, ',', '.') .'</span>';
        } else{
            echo '<span>-</span>';
		} ?>

    </div>
    <div class="col s12 m6">
        <?php foreach ($cast['crew'] as $crew_member){
            if ($crew_member['job'] == 'Director' || $crew_member['job'] == 'Writer' || $crew_member['job'] == 'Screenplay' || $crew_member['job'] == 'Author'){
                echo '<div class="crew_member"><h5>'. $crew_member['job'] .'</h5><span>'.$crew_member['name'] .'</span></div>';
			}
        } ?>

        <h5>Production companies</h5>
        <?php if ($movie['production_companies']){
			$total = count($movie['production_companies']);
			$count = 0;
			foreach ($movie['production_companies'] as $company){
				$count++; ?>
                <span><?= $company['name'] . ($count !== $total ? ',' : '') ?></span>
	            <?php
			}
        } else {
            echo '<span>-</span>';
    	} ?>

        <h5>Genres</h5>
        <?php if ($movie['genres']){
            foreach ($movie['genres'] as $genre){
                echo '<span class="chip accent">'. $genre['name'] .'</span>';
			}
        } else {
            echo '<span>-</span>';
        } ?>

        <h5>Homepage</h5>
        <?php if ($movie['homepage']){
            echo '<a class="truncate" href="'. $movie['homepage'] .'" target="_blank">'. $movie['homepage'] .'</a>';
        } else{
            echo '<span>-</span>';
        } ?>
    </div>
</div>
