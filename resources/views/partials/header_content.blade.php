<div class="item" style="background-image: url('https://image.tmdb.org/t/p/w1280{{$movie['backdrop_path']}}')">
    <div class="overlay">
        <a class="content" href="{{ URL::to('movie/'.$movie['id']) }}">

			<!-- TITLE -->
			<h1><?= $movie['title']?></h1>

			<!-- VOTES & STARS -->
            <div class="chip">
                <i class="material-icons tiny">star</i>
                <span>{{ round($movie['vote_average'], 1) }}</span>
            </div>
            <span> | {{ $movie['vote_count'] }} Votes</span>

			<!-- MORE INFO -->
            <div class="actions hide-on-med-and-down">
                <div href="#" class="button accent transition">More info</div>
            </div>
        </a>
    </div>
</div>
