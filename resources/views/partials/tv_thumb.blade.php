<div class="item">
    <a href="{{ URL::to('tv/'.$tv['id']) }}" class="col s12 movie_thumb">
        <div class="card_new">
            @if (Auth::check())
                <div class="card-actions">
                {{-- <i class="material-icons">remove_red_eye</i> --}}
                @if ($tv['watchlist'] == 0)
                    <i class="material-icons tooltipped" data-tooltip="Add to watchlist"  onclick="event.preventDefault(); add_to_watchlist(this, {{ Auth::id() }} ,'tv', '{{ $tv['id'] }}', '{{ $tv['name'] }}', '{{ $tv['poster_path'] }}', '{{ 'tv/'.$tv['id'] }}')">playlist_add</i>
                @else
                    <i class="material-icons tooltipped" data-tooltip="Remove from watchlist"  onclick="event.preventDefault(); remove_from_watchlist(this, {{ Auth::id() }} ,'tv', '{{ $tv['id'] }}')">playlist_add_check</i>
                @endif
                </div>
            @endif
            <div class="card-image z-depth-2">
                <img src="http://image.tmdb.org/t/p/w300{{$tv['poster_path']}}">
            </div>
            <div class="card-content">
                <h4 class="truncate">{{$tv['name']}}</h4>
                {{-- <div class="btn-floating rating"><span>{{$tv['vote_average']}}</span></div> --}}
            </div>
        </div>
    </a>
</div>
