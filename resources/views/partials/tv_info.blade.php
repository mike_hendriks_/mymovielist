<div class="row">
    <div class="col s12">
        <h3>Facts</h3>
    </div>
    <div class="col s12 m6">
        <h5 class="no_margin_top">Release date</h5>
        @if ($tv['first_air_date'])
            <span>{{$tv['first_air_date']}}</span>
        @else
            <span>-</span>
        @endif

        <h5>Status</h5>
        @if ($tv['status'])
            <span>{{$tv['status']}}</span>
        @else
            <span>-</span>
        @endif

        <h5>Runtime</h5>
        @if ($tv['episode_run_time'])
            <span>{{$tv['episode_run_time']{0} }} Minutes</span>
        @else
            <span>-</span>
        @endif

        <h5>Homepage</h5>
        @if ($tv['homepage'])
            <a class="truncate" href="{{$tv['homepage']}}" target="_blank">{{$tv['homepage']}}</a>
        @else
            <span>-</span>
		@endif
    </div>

    <div class="col s12 m6">
        @foreach ($cast['crew'] as $crew_member)
            @if ($crew_member['job'] == 'Director' || $crew_member['job'] == 'Writer' || $crew_member['job'] == 'Screenplay' || $crew_member['job'] == 'Author' || $crew_member['job'] == 'Creator')
                <div class="crew_member">
                    <h5>{{$crew_member['job']}}</h5>
                    <span>{{$crew_member['name']}}</span>
                </div>
            @endif
        @endforeach

		<h5>Production companies</h5>
        @if ($tv['production_companies'])
            @php
    			$total = count($tv['production_companies']);
    			$count = 0;
            @endphp
			@foreach ($tv['production_companies'] as $company)
				@php
                    $count++
                @endphp
                <span>{{ $company['name'] . ($count !== $total ? ',' : '') }}</span>
            @endforeach
        @else
            <span>-</span>
        @endif

        <h5>Genres</h5>
        @if ($tv['genres'])
			@foreach ($tv['genres'] as $genre)
                <span class="chip accent">{{$genre['name']}}</span>
            @endforeach
        @else
            <span>-</span>
        @endif

    </div>
</div>
