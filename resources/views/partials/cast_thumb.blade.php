<!-- CAST THUMBNAIL -->
<div class="col s12 cast_member">
    <div class="card_new cast">
		<!-- IMAGE -->
        <div class="card-image z-depth-2">
            @if (!$cast_member['profile_path'])
                <img src="https://assets.tmdb.org/assets/7f29bd8b3370c71dd379b0e8b570887c/images/no-poster-w185-v2.png">
            @else
                <img src="http://image.tmdb.org/t/p/w300{{ $cast_member['profile_path'] }}">
            @endif
        </div>
		<!-- INFO -->
        <div class="card-stacked">
            <div class="card-content">
                <h5>{{ $cast_member['character'] }}</h5>
                <h6>{{ $cast_member['name'] }}</h6>
            </div>
        </div>
    </div>
</div>
