<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="theme-color" content="#607d8b">

        <title>My Movielist</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <!-- Stylesheets -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/css/materialize.min.css">
        {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"> --}}
        {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"> --}}
        <link type="text/css" rel="stylesheet" href="{{ URL::to('/css/app.css') }}"  media="screen,projection"/>
    </head>
    <body>
        <!-- TRAILER WINDOW -->
    	<div id="trailer_container">
    		<i class="material-icons close">close</i>
    		<div id="video_view"></div>
    	</div>

        <!-- NAVIGATION -->
        <nav class="home z-depth-2">
            <div class="nav-wrapper">
                <ul class="brand-logo left">
                    <li id="logo">
                        <a href="<?= URL::to('/');?>">
                            <svg fill="#fff" viewBox="0 0 279.79 201.66"><g id="Layer_2" data-name="Layer 2">
                                <path class="cls-1" d="M108.78 54.5a23.78 23.78 0 0 0-41.18 0L3.22 166a23.78 23.78 0 0 0 20.59 35.66h128.75A23.78 23.78 0 0 0 173.15 166zM29.83 188.08h-17v-16.7h17zm33.4 0h-16.7v-16.7h16.7zm33.4 0h-16.7v-16.7h16.7zm33.4 0h-16.7v-16.7H130zm33.4 0h-16.7v-16.7h16.7zm7.57-40.92a23.78 23.78 0 0 0 41.18 0l64.37-111.5A23.78 23.78 0 0 0 256 0H127.23a23.78 23.78 0 0 0-20.59 35.66zm79-133.58h17v16.7h-17zm-33.4 0h16.7v16.7h-16.7zm-33.4 0h16.7v16.7h-16.7zm-33.4 0h16.7v16.7h-16.7zm-33.4 0h16.7v16.7h-16.7z" id="Layer_1-2" data-name="Layer 1"/></g>
                            </svg>
                        </a>
                    </li>
                </ul>

                @if (Auth::check())
                    <div class="fixed-action-btn click-to-toggle" id="user_info">
                        <p>Hey {{ ucfirst(Auth::user()['first_name']) }}</p>
                        <a class="btn-floating transparent">
                            <img src="{{Auth::user()['user_img']}}" alt="{{ ucfirst(Auth::user()['first_name']) }}">
                        </a>
                        <ul class="dropdown">
                            <li><a href="{{URL::to('/logout')}}" class="btn-floating red tooltipped z-depth-5" data-tooltip="Logout"><i class="material-icons">exit_to_app</i></a></li>
                        </ul>
                        <a href="{{ URL::to('/watchlist') }}" class="btn-floating watchlist_btn accent"><i class="material-icons">ondemand_video</i></a>
                    </div>
                @else
                    <a href="{{ URL::to('/auth/google')}}" id="login_button">Login</a>
                @endif

                <ul id="nav_search">
                    <i class="material-icons" id="search_icon">search</i>

                    <form id="search_overlay" class="row">
                        <i class="material-icons right" id="search_close">close</i>
                        <div class="input-field">
                            <i class="material-icons prefix">search</i>
                            <input id="search" type="text" autocomplete="off" placeholder="Search..">
                        </div>
                        <div id="results">
                            <div class="row">
                                <div class="col s12 m6 l6">
                                    <h3>Movie</h3>
                                    <div id="result_movie">

                                    </div>
                                </div>
                                <div class="col s12 m6 l6">
                                    <h3>Tv</h3>
                                    <div id="result_tv_show">

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </ul>
            </div>
        </nav>

        <div class="row">
            @yield('content')
        </div>

        {{-- Scripts --}}
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/js/materialize.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="{{ URL::to('libs/jquery.quickfit.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>
        <script type="text/javascript" src="{{ URL::to('js/app.js') }}"></script>

        <script type="text/javascript">

            var base_url = '{{ URL::to('/') }}/';

            function add_to_watchlist(e, user_id, type, item_id, item_title, item_img, item_url) {
            	$.ajax({
            		type: "POST",
            		url: base_url + "watchlist/add_to_watchlist",
            		data: {"_token": "{{ csrf_token() }}", user_id, type, item_id, item_title, item_img, item_url},
            		success: function(data){
            			console.log(data);
            			$(e).replaceWith('<i class="material-icons tooltipped" data-tooltip="Remove from watchlist" onclick="event.preventDefault(); remove_from_watchlist(this, '+user_id+', \''+type+'\', \''+item_id+'\', \''+item_title+'\', \''+item_img+'\', \''+item_url+'\');">playlist_add_check</i>');
            			$('.tooltipped').tooltip('remove');
            			Materialize.toast('Added to watchlist', 4000) // 4000 is the duration of the toast
              			$('.tooltipped').tooltip({delay: 50});
            		}
            	});
            }

            function remove_from_watchlist(e, user_id, type, item_id, item_title, item_img, item_url) {
            	$.ajax({
            		type: "POST",
            		url: base_url + "watchlist/remove_from_watchlist",
            		data: {"_token": "{{ csrf_token() }}", user_id, type, item_id},
            		success:
            		function(data){
            			console.log(data);
            			$(e).replaceWith('<i class="material-icons tooltipped" data-tooltip="Add to watchlist" onclick="event.preventDefault(); add_to_watchlist(this, '+user_id+', \''+type+'\', \''+item_id+'\', \''+item_title+'\', \''+item_img+'\', \''+item_url+'\');">playlist_add</i>');
            			$('.tooltipped').tooltip('remove');
            			Materialize.toast('Removed from watchlist', 4000) // 4000 is the duration of the toast
            			$('.tooltipped').tooltip({delay: 50});
            		}
            	});
            }

            function remove_from_html(e) {
            	$(e).closest('.movie_thumb').fadeOut(300, function(){
            		$(this).remove();
            	});
            }
        </script>

    </body>
</html>
