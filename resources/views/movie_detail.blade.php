@extends('templates.app')

@section('content')
    <header class="small z-depth-2" style="background-image: url('https://image.tmdb.org/t/p/original{{$movie['backdrop_path']}}')">
    	<div class="overlay"></div>
    </header>

    <div class="container detail">
    	<div class="row">

    		<!-- POSTER -->
    		<img src="https://image.tmdb.org/t/p/w300{{$movie['poster_path']}}" class="poster z-depth-2 hide-on-med-and-down">

    		<!-- MOVIE INFO -->
    		<div class="header_content">

    			<!-- TITLE -->
    			<div id="fittin">
    				<h1>{{$movie['title']}}</h1>
    			</div>

    			<!-- GENRES -->
    			<?php foreach ($movie['genres'] as $i => $genre_id):
    				if($i==2) break; ?>
    				<div class="chip hide-on-med-and-down">
    					<?php echo $genre_id['name'] ?>
    				</div>
    			<?php endforeach; ?>

    			<!-- RATING -->
    			<div class="chip">
    				<i class="material-icons tiny">star</i>
    				<span><?= round($movie['vote_average'], 1) ?></span>
    			</div>
    			<span> | {{$movie['vote_count']}} Votes</span>

    			<!-- TIMES -->
    			<div class="date hide-on-med-and-down">
    				<i class="material-icons small">date_range</i>
    				<span>{{$movie['release_date']}}</span>
    				<i class="material-icons small">access_time</i>
    				<span>{{$movie['runtime']}} min</span>
    			</div>

    			<!-- WATCHLIST BUTTON -->
    			@if (Auth::check())
                    @if ($movie['watchlist'] == 0)
    					<a class="btn-floating btn-large waves-effect waves-light accent watchlist_circle_btn tooltipped" data-tooltip="Add to watchlist" >
    						<i class="material-icons right" onclick="event.preventDefault(); add_to_watchlist(this, {{ Auth::id() }} ,'movie', '{{ $movie['id'] }}', '{{ $movie['title'] }}', '{{ $movie['poster_path'] }}', '{{ 'movie/'.$movie['id'] }}')">playlist_add</i>
    					</a>
    				@else
    					<a class="btn-floating btn-large waves-effect waves-light accent watchlist_circle_btn tooltipped" data-tooltip="Remove from watchlist" >
    						<i class="material-icons right" onclick="event.preventDefault(); remove_from_watchlist(this, {{ Auth::id() }} ,'movie', '{{ $movie['id'] }}')">playlist_add_check</i>
    					</a>
    			    @endif
                @endif

				<!-- PLOT -->
				<div class="plot">
					<h3>Plot</h3>
					<p>
						{{ $movie['overview'] }}
					</p>

					<!-- TRAILERS -->
                    @foreach ($trailers as $i => $trailer)
                        @if ($i <= 2)
                            <a href="#" class="trailer_button popup_trailer button accent waves-effect" data-link="https://www.youtube.com/watch?v={{$trailer['source']}}">Trailer {{ $i+1 }}</a>
                        @endif
                    @endforeach
				</div>
			</div>
		</div>
	</div>

	<div class="container main_content margin-top-50">
		<!-- CAST -->
		<h3>Cast</h3>
		<div class="block">
			<!-- CAST SLIDER -->
			<div class="slider">
                @foreach ($cast['cast'] as $cast_member)
                    @include('partials.cast_thumb')
                @endforeach
			</div>
		</div>
	</div>

	<!-- FACTS -->
	<div class="container_fluid object_info">
		<!-- CONTAINER BACKGROUND -->
		<div class="background" style="background-image: url('https://image.tmdb.org/t/p/original{{ $movie['backdrop_path'] }}')"></div>
		<div class="container z-depth-2">
			<!-- FACTS DATA -->
            @include('partials.movie_info')
		</div>
	</div>

    @if ($similar)
    	<!-- SIMILAR MOVIES -->
    	<div class="container main_content">
    			<h3>Similar movies</h3>
                @foreach ($similar as $i => $movie)
                    @if ($i <= 3)
                        @include('partials.similar_movie_thumb')
                    @endif
                @endforeach

    	</div>
    @endif
@endsection
