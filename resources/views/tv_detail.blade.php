@extends('templates.app')

@section('content')
    <header class="small z-depth-2" style="background-image: url('https://image.tmdb.org/t/p/original{{$tv['backdrop_path']}}')">
    	<div class="overlay"></div>
    </header>

    <div class="container detail">
    	<div class="row">

    		<!-- POSTER -->
    		<img src="https://image.tmdb.org/t/p/w300{{$tv['poster_path']}}" class="poster z-depth-2 hide-on-med-and-down">

    		<!-- MOVIE INFO -->
    		<div class="header_content">

    			<!-- TITLE -->
    			<div id="fittin">
    				<h1>{{$tv['name']}}</h1>
    			</div>

    			<!-- GENRES -->
    			<?php foreach ($tv['genres'] as $i => $genre_id):
    				if($i==2) break; ?>
    				<div class="chip hide-on-med-and-down">
    					<?php echo $genre_id['name'] ?>
    				</div>
    			<?php endforeach; ?>

    			<!-- RATING -->
    			<div class="chip">
    				<i class="material-icons tiny">star</i>
    				<span><?= round($tv['vote_average'], 1) ?></span>
    			</div>
    			<span> | {{$tv['vote_count']}} Votes</span>

    			<!-- TIMES -->
                <div class="date hide-on-med-and-down">
                    <i class="material-icons small">date_range</i>
                    <span>{{$tv['first_air_date']}}</span>
                    @if ($tv['episode_run_time'])
                        <i class="material-icons small">access_time</i>
                        <span>{{$tv['episode_run_time'][0]}} min</span>
                    @endif
                </div>

    			<!-- WATCHLIST BUTTON -->
    			@if (Auth::check())
                    @if ($tv['watchlist'] == 0)
    					<a class="btn-floating btn-large waves-effect waves-light accent watchlist_circle_btn tooltipped" data-tooltip="Add to watchlist" >
    						<i class="material-icons right" onclick="event.preventDefault(); add_to_watchlist(this, {{ Auth::id() }} ,'movie', '{{ $tv['id'] }}', '{{ $tv['name'] }}', '{{ $tv['poster_path'] }}', '{{ 'movie/'.$tv['id'] }}')">playlist_add</i>
    					</a>
    				@else
    					<a class="btn-floating btn-large waves-effect waves-light accent watchlist_circle_btn tooltipped" data-tooltip="Remove from watchlist" >
    						<i class="material-icons right" onclick="event.preventDefault(); remove_from_watchlist(this, {{ Auth::id() }} ,'movie', '{{ $tv['id'] }}')">playlist_add_check</i>
    					</a>
    			    @endif
                @endif

				<!-- PLOT -->
				<div class="plot">
					<h3>Plot</h3>
					<p>
						{{ $tv['overview'] }}
					</p>

					<!-- TRAILERS -->
                    @foreach ($trailers as $i => $trailer)
                        @if ($trailer['site'] == 'Youtube')
                            @if ($i <= 2)
                                <a href="#" class="trailer_button popup_trailer button accent waves-effect" data-link="https://www.youtube.com/watch?v={{$trailer['key']}}">Trailer {{ $i+1 }}</a>
                            @endif
                        @endif
                    @endforeach
				</div>
			</div>
		</div>
	</div>

	<div class="container main_content margin-top-50">
		<!-- CAST -->
		<h3>Cast</h3>
		<div class="block">
			<!-- CAST SLIDER -->
			<div class="slider">
                @foreach ($cast['cast'] as $cast_member)
                    @include('partials.cast_thumb')
                @endforeach
			</div>
		</div>
	</div>

	<!-- FACTS -->
	<div class="container_fluid object_info">
		<!-- CONTAINER BACKGROUND -->
		<div class="background" style="background-image: url('https://image.tmdb.org/t/p/original{{ $tv['backdrop_path'] }}')"></div>
		<div class="container z-depth-2">
			<!-- FACTS DATA -->
            @include('partials.tv_info')
		</div>
	</div>

	<!-- SIMILAR MOVIES -->
	<div class="container main_content">
        @if ($similar)
			<h3>Similar tv shows</h3>
            @foreach ($similar as $i => $tv)
                @if ($i <= 3)
                    @include('partials.similar_tv_thumb')
                @endif
            @endforeach

        @endif
	</div>
@endsection
