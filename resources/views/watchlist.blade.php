@extends('templates.app')

@section('content')
    <header class="z-depth-2 smaller">
    	<div class="item" style="background-image: url('https://image.tmdb.org/t/p/original{{$header}}')">
    	    <div class="overlay"></div>
    	</div>
    </div>

    <div class="row">
        <div class="container">
        	<h2>My watchlist</h2>
        	<div class="item_container">
                @foreach ($watchlist as $item)
                    @if ($item['type'] == 'tv')
    					<a href="{{ URL::to('tv/'.$item['item_id'])}}" class="col s6 m6 l3 movie_thumb">
    						<div class="card_new">
    							<div class="card-actions">
    								<!-- <i class="material-icons">remove_red_eye</i> -->
    								<i class="material-icons" onclick="event.preventDefault(); remove_from_watchlist(this, {{ Auth::id() }} ,'tv', '{{$item['item_id'] }}'); remove_from_html(this)">playlist_add_check</i>
    							</div>
    							<div class="card-image z-depth-2">
    								<img src="https://image.tmdb.org/t/p/w300{{$item['item_img']}}">
    							</div>
    							<div class="card-content">
    								<h4 class="truncate">{{$item['item_title']}}</h4>
    							</div>
    						</div>
    					</a>
                    @elseif ($item['type'] == 'movie')
						<a href="{{ URL::to('movie/'.$item['item_id']) }}" class="col s6 m6 l3 movie_thumb">
							<div class="card_new">
								<div class="card-actions">
									<!-- <i class="material-icons">remove_red_eye</i> -->
									<i class="material-icons" onclick="event.preventDefault(); remove_from_watchlist(this, {{ Auth::id() }} ,'movie', '{{$item['item_id']}}'); remove_from_html(this)">playlist_add_check</i>
								</div>
								<div class="card-image z-depth-2">
									<img src="https://image.tmdb.org/t/p/w300{{$item['item_img']}}">
								</div>
								<div class="card-content">
									<h4 class="truncate">{{ $item['item_title'] }}</h4>
								</div>
							</div>
						</a>
                    @endif
                @endforeach
        	</div>
    	</div>
    </div>
@endsection
