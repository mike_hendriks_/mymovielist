@extends('templates.app')

@section('content')
    <header class="z-depth-2 slider_big">
        @foreach ($upcoming_movies as $i => $movie)
            @if ($i <= 6)
                @include('partials.header_content')
            @endif
        @endforeach
    </header>

    <div class="container">

    	<h2>In theaters now</h2>
    	<div class="item_container thumb_slider">
            @foreach ($now_playing_movies as $movie)
                @include('partials.movie_thumb')
            @endforeach
        </div>


    	<h2>Upcoming movies</h2>
    	<div class="item_container thumb_slider">
            @foreach ($upcoming_movies as $movie)
                @include('partials.movie_thumb')
            @endforeach
    	</div>


    	<h2>Series airing today</h2>
    	<div class="item_container thumb_slider">
            @foreach ($airing_today_tv as $tv)
                @include('partials.tv_thumb')
            @endforeach
    	</div>
    </div>

@endsection
