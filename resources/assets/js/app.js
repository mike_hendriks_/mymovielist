$(document).ready(function(){
	$("#nav_search form").submit(function(e){
		return false;
	});

	$("#search").keyup(function(e){
		var q = $("#search").val();

		if (q.length >= 3) {
			var settings = {
				"async": true,
				"crossDomain": true,
				"url": "https://api.themoviedb.org/3/search/multi?&api_key=e8dab41147676579acffe33ed9836c7b&query="+q,
				"method": "GET",
				"processData": false,
				"data": "{}"
			}

			$.ajax(settings).done(function (response) {
				// console.log(response);
				$("#result_tv_show").empty();
				$("#result_movie").empty();
				// $("#result_person").empty();

				$.each(response.results, function(i,item){

					if (item.media_type === 'tv') {
						if (item.poster_path) {
							$("#result_tv_show").append("<a href='" + base_url + "tv/" + item.id + "'><div class='card horizontal'><div class='card-image'><img src='https://image.tmdb.org/t/p/w300"+ item.poster_path +"' alt='"+item.name+"'></div><div class='card-stacked'><div class='card-content'><h3 class='header'>"+item.name+"</h3><p>"+item.overview+"</p></div></div></div></a>");
						// } else {
						// 	$("#result_tv_show").append("<a href='" + base_url + "tv/" + item.id + "'><div class='card horizontal'><div class='card-image'><img src='https://assets.tmdb.org/assets/7f29bd8b3370c71dd379b0e8b570887c/images/no-poster-w185-v2.png' alt='"+item.name+"'></div><div class='card-stacked'><div class='card-content'><h3 class='header'>"+item.name+"</h3><p>"+item.overview+"</p></div></div></div></a>");
						}
					} else if (item.media_type === 'movie') {
						if (item.poster_path) {
							$("#result_movie").append("<a href='" + base_url + "movie/" + item.id + "'><div class='card horizontal'><div class='card-image'><img src='https://image.tmdb.org/t/p/w300"+ item.poster_path +"' alt='"+item.title+"'></div><div class='card-stacked'><div class='card-content'><h3 class='header'>"+item.title+"</h3><p>"+item.overview+"</p></div></div></div></a>");
						// } else {
						// 	$("#result_movie").append("<a href='" + base_url + "movie/" + item.id + "'><div class='card horizontal'><div class='card-image'><img src='https://assets.tmdb.org/assets/7f29bd8b3370c71dd379b0e8b570887c/images/no-poster-w185-v2.png' alt='"+item.title+"'></div><div class='card-stacked'><div class='card-content'><h3 class='header'>"+item.title+"</h3><p>"+item.overview+"</p></div></div></div></a>");
						}
					// } else if (item.media_type === 'person') {
					// 	if (item.profile_path) {
					// 		$("#result_person").append("<a href='#'><div class='card horizontal'><div class='card-image'><img src='https://image.tmdb.org/t/p/w600_and_h900_bestv2"+ item.profile_path +"' alt='"+item.name+"'></div><div class='card-stacked'><div class='card-content'><h3 class='header'>"+item.name+"</h3></div></div></div></a>");
					// 	} else {
					// 		console.log();
					// 		$("#result_person").append("<a href='#'><div class='card horizontal'><div class='card-image'><img src='https://assets.tmdb.org/assets/7f29bd8b3370c71dd379b0e8b570887c/images/no-poster-w185-v2.png' alt='"+item.name+"'></div><div class='card-stacked'><div class='card-content'><h3 class='header'>"+item.name+"</h3></div></div></div></a>");
					// 	}
					};
				});
			});
		}
	});


	// Search open
	$('#search_icon').on('click', function(){
		open_search();
	})

	// Search close
	$('#search_close').on('click', function(){
		close_search();
	})

	// Close search window if escape key is pressed
	$(document).keyup(function(e) {
		if (e.keyCode == 27 && $('#search_overlay').is(":visible")) {
			close_search();
		}
	});

	// Open popup with trailer link
	$(".popup_trailer").on('click', function () {
		var $this = $(this);
		var $myId = get_trailer_id($this.data('link'));
		var $iframe = '<iframe width="560" height="315" src="https://www.youtube.com/embed/'
		+ $myId + '?autoplay=1" frameborder="0" allowfullscreen></iframe>';
		$("#video_view").append($iframe);
		$("#trailer_container").fadeIn();
		$("body").css('overflow','hidden');
	});
	// Close trailer window
	$('#trailer_container').on('click', '.close, #video_view', function(){
		close_trailer_window();
	});

	// Initiate SideNavigation on mobile
	$(".button-collapse").sideNav();

	// Match the height of every thumbnail
	$('.movie_thumb img').matchHeight();

	// Initiate sliders
	// Cast slider
	$('.slider').owlCarousel({
		loop:false,
		margin:0,
		responsiveClass:true,
		nav: false,
		dots: true,
		responsive:{
			0:{
				items:2,
			},
			600:{
				items:3,
			},
			800:{
				items:4,
			},
			992:{
				items:5,
			}
		}
	});
	// Thumb slider
	$('.thumb_slider').owlCarousel({
		responsiveClass:true,
		nav: false,
		dots: true,
		loop: true,
		autoplay: true,
		lazyLoad:true,
		lazyLoadEager: 2,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		responsive:{
			0:{
				items:2,
			},
			600:{
				items:3,
			},
			800:{
				items:4,
			},
			992:{
				items:4,
			}
		}
	});

	// Initiate header Slider
	$('.slider_big').owlCarousel({
		loop:true,
		margin:0,
		singleItem: true,
		items: 1,
		dots: true,
		autoplay: true,
		autoplayTimeout: 5500,
		autoplayHoverPause: true
	});

	// Set slider navigation ( CAST )
	var owl = $('.slider');
	owl.owlCarousel();
	// Go to the next item
	$('.next').click(function() {
		owl.trigger('next.owl.carousel');
	})
	// Go to the previous item
	$('.prev').click(function() {
		owl.trigger('prev.owl.carousel', [300]);
	})

	// If title reacher 2 lines
	$(window).on('resize', function() {
		$('.header_content h1').quickfit({ max: 63, min: 22, truncate: true, tolerance: 0.05 });
		$('header h1').quickfit({ max: 63, min: 22, truncate: true, tolerance: 0.05 });
	});
	$(window).trigger('resize');
})


function close_search() {
	$('#search_overlay').slideUp(200);
	$('body').css('overflow', 'auto');
}

function open_search() {
	$('#search_overlay').slideDown(200);
	$('body').css('overflow', 'hidden');
	$('#search_overlay input').trigger('focus');
}

function get_trailer_id(url) {
	var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
	var match = url.match(regExp);

	if (match && match[2].length == 11) {
		return match[2];
	} else {
		return 'error';
	}
}

function close_trailer_window() {
	$("#video_view").html("");
	$('body').css("overflow", 'auto');
	$('#trailer_container').fadeOut();
}
